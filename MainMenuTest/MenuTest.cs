﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChpThreeExes;

namespace MainMenuTest
{
    [TestClass]
    public class MenuTest
    {
        [TestMethod]
        public void MainMenuKeyReturn()
        {
            string testKey = "7".ToUpper();
            MainMenu mainMenu = new MainMenu();
            mainMenu.MainMenuOptions();
            Assert.AreEqual("a", testKey);

        }
    }
}
