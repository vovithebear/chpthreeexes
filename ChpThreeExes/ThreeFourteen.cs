﻿using System;

namespace ChpThreeExes
{
    class ThreeFourteen
    {
        int numOne;
        int numTwo;
        int numThree;
        int numFour;

        public void DisplayThreeFourteen()
        {
            Console.Write("Exercise 3.14\n\n");
            Console.Write("Please choose the first number:  ");
            numOne = int.Parse(Console.ReadLine());

            Console.Write("\nPlease choose the second number: ");
            numTwo = int.Parse(Console.ReadLine());

            Console.Write("\nPlease choose the third number: ");
            numThree = int.Parse(Console.ReadLine());

            Console.Write("\nPlease choose the forth number: ");
            numFour = int.Parse(Console.ReadLine());

            DoSpacing();
        }

        private void DoSpacing()
        {
            Console.Write(($"\nThe first pair is {numOne} , {numTwo}") + " and " + ($"the next pair is {numThree} , {numFour}\n\n"));
        }
    }
}
