﻿using System;

namespace ChpThreeExes
{
    public class MainMenu
    {
        private ConsoleKeyInfo chosenKey;

        private string pressedKey;

        public string MainMenuOptions()
        {
            Console.WriteLine("Please choose from the options below which exercises you would like to do (Please choose the lettered options ): ");
            
            Console.WriteLine("\n a) Exercise 3.14\n");
           
            Console.WriteLine(" b) Exercise 3.15\n");
         
            Console.WriteLine(" c) Exercise 3.16\n");

            Console.WriteLine(" d) Exercise 3.17\n");

            Console.WriteLine(" e) Exercise 3.18\n");

            Console.WriteLine(" f) Exercise 3.24\n");

            Console.WriteLine(" g) Exercise 3.25\n");

            Console.WriteLine(" h) Exercise 3.26\n");

            Console.WriteLine(" i) Exercise 3.27\n");

            Console.WriteLine(" j) Exercise 3.28\n");

            Console.WriteLine(" k) Exercise 3.29\n");

            Console.WriteLine(" l) Exercise 3.30\n");

            Console.WriteLine(" m) Exercise 3.31\n");

            Console.WriteLine(" n) Exercise 3.32\n");

            chosenKey = Console.ReadKey();

            pressedKey = chosenKey.Key.ToString();

            return pressedKey;
        }
    }
}
